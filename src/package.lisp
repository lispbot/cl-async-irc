;;;; package.lisp

(defpackage #:cl-async-irc
  (:use #:cl #:alexandria)
  (:nicknames #:irc)
  (:export #:connection
           #:connect
           #:disconnect
           #:nick
           #:user
           #:join
           #:part
           #:quit
           #:topic
           #:pong

           #:cancle-ping-timeout
           #:ping-timeout

           #:privmsg
           #:action

           #:add-handler
           #:remove-handler

           #:message
           #:message-prefix
           #:message-command
           #:message-params
           #:message-user-nick
           #:message-user-host
           #:message-user-user))


(in-package :cl-user)
(defpackage cl-async-irc/parsing-test
  (:use :cl
        :cl-async-irc
        :prove))

(in-package :cl-async-irc/parsing-test)

(plan 3)

(defun test-parse (str exp-prefix exp-command exp-params)
  (let ((msg (cl-async-irc::parse-message str)))
    (subtest "Parsing test"
      (is (irc:message-prefix msg) exp-prefix)
      (is (irc:message-command msg) exp-command)
      (is (irc:message-params msg) exp-params))))


(test-parse ":BelWue.DE 001 blahfoo :Welcome to the Internet Relay Network blahfoo"
            '("BelWue.DE") :rpl-welcome '("blahfoo" "Welcome to the Internet Relay Network blahfoo"))

(test-parse ":blahfoo!~blah@example.com JOIN :#somechannel"
            '("blahfoo" "~blah" "example.com") :join '("#somechannel"))

(test-parse ":BelWue.DE 332 blahfoo #somechannel :This is my topic"
            '("BelWue.DE") :rpl-topic '("blahfoo" "#somechannel" "This is my topic"))

(finalize)

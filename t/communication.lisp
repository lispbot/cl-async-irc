(in-package :cl-user)
(defpackage cl-async-irc/communication-test
  (:use :cl
        :cl-async-irc
        :prove))

(in-package :cl-async-irc/communication-test)

(plan 4)

(defvar *seen-commands*)
(defvar *client-connection*)

(defun read-handler (sock data)
  (let ((str (string-trim '(#\Newline #\Return) (babel:octets-to-string data))))
    (when *seen-commands*
      (is str (car *seen-commands*))
      (funcall (cdr *seen-commands*) sock))))

(defun start-tcp-server ()
  (bb:with-promise (resolve reject)
    (as:tcp-server "127.0.0.1" 6667
                   #'read-handler
                   :event-cb (lambda (ev) (reject ev))
                   :connect-cb
                   (lambda (sock)
                     (declare (ignore sock))
                     (resolve nil)))))

(defun after-nick (sock)
  (as:write-socket-data sock (babel:string-to-octets (format nil "PING :foobar~%")))
  (setf *seen-commands* '("PONG :foobar" . after-pong)))

(defun after-pong (sock)
  (declare (ignore sock))
  (irc:join *client-connection* "#testchan")
  (setf *seen-commands* '("JOIN :#testchan" . after-join)))

(defun after-join (sock)
  (declare (ignore sock))
  (setf *seen-commands* nil))

(defun event-handler (ev)
  (handler-case (error ev)
    (irc:ping-timeout () (pass "Ping timeout"))
    (t () (fail "Error occured"))))

(as:with-event-loop (:catch-app-errors #'event-handler :send-errors-to-eventcb nil)
  (bb:catcher
   (bb:wait (start-tcp-server)
     (setf *seen-commands* '("NICK :test" . after-nick))
     (diag "Connected."))
   (t (err) (signal err)))
  (setf *client-connection*
        (irc:connect "127.0.0.1" 6667 :nick "test"
                     :ping-timeout 2
                     :event-cb (lambda (ev)
                                 (if (eq (type-of ev) 'irc:ping-timeout)
                                     (pass "Ping timeout")
                                     (fail "Error occured"))
                                 (format t "Error: ~a~%" ev))))
  (as:with-delay (3) (as:exit-event-loop)))

(finalize)

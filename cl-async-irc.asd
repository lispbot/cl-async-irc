;;;; cl-async-irc.asd

(asdf:defsystem #:cl-async-irc
  :description "IRC Client implementation using cl-async"
  :version "0.9.0"
  :author "Hans-Peter Deifel <hpd@hpdeifel.de>"
  :license "MIT"
  :serial t
  :pathname "src/"
  :components ((:file "package")
               (:file "cl-async-irc"))
  :depends-on (#:cl-async
               #:cl-async-ssl
               #:babel
               #:alexandria
               #:split-sequence
               #:blackbird)
  :in-order-to ((test-op (test-op cl-async-irc-test))))

(asdf:defsystem #:cl-async-irc-test
  :description "Tests for cl-async-irc"
  :author "Hans-Peter Deifel <hpd@hpdeifel.de>"
  :license "MIT"
  :serial t
  :pathname "t/"
  :components ((:test-file "parsing")
               (:test-file "communication"))
  :depends-on (#:cl-async-irc
               #:prove
               #:cl-async
               #:blackbird
               #:babel)
  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c) (funcall (intern #.(string :run) :prove) c)))

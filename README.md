# cl-async-irc

A thin wrapper around irc using cl-async for the event loop

## Running the tests

Load `cl-async-irc` and install `prove` via quicklisp or otherwise.
Then run:

    (asdf:test-system :cl-async-irc-test)
